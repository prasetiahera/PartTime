package com.parttime.parttime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;


public class CategoryActivity extends AppCompatActivity  {

    private LinearLayout llTeacher, llFood, llCleaning, llTransportation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        llCleaning = (LinearLayout) findViewById(R.id.category_cleaning);
        llFood = (LinearLayout) findViewById(R.id.category_food);
        llTeacher = (LinearLayout) findViewById(R.id.category_teacher);
        llTransportation = (LinearLayout) findViewById(R.id.category_transportation);

        llCleaning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(CategoryActivity.this, "You click Cleaning Category", Toast.LENGTH_LONG).show();
            }
        });

        llFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(CategoryActivity.this, "You click Food Category", Toast.LENGTH_LONG).show();
            }
        });

        llTeacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(CategoryActivity.this, "You click Teacher Category", Toast.LENGTH_LONG).show();
            }
        });

        llTransportation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(CategoryActivity.this, "You click Transportation Category", Toast.LENGTH_LONG).show();
            }
        });

    }

}
